# alt-calc-electron

This is an electron calculator app.

## Repositories

- The primary repository is on GitLab -- [gitlab.com/altonaga/alt-calculator-electron](https://gitlab.com/altonaga/alt-calculator-electron)
  - This is the repo, where this project is managed.

- A secondary repository is located at GitHub -- [github.com/altonaga/alt-calc-electron](https://github.com/altonaga/alt-calc-electron)
  - This repo is for maintaining a master branch as a reference.
